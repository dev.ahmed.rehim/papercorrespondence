﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace PaperCorrespondence
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}

		public override void Init()
		{
			base.PostAuthenticateRequest += OnAfterAuthenticateRequest;
		}

		private void OnAfterAuthenticateRequest(object sender, EventArgs eventArgs)
		{
			var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (cookie != null)
			{
				var decodedTicket = ((FormsIdentity)HttpContext.Current.User.Identity).Ticket;
				var roles = decodedTicket.UserData.Split(',');
				var principal = new GenericPrincipal(HttpContext.Current.User.Identity, roles);
				HttpContext.Current.User = principal;
			}
		}
	}
}
