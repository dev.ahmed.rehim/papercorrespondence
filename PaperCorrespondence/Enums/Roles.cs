﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaperCorrespondence.Enums
{
	public enum Roles
	{
		entrance,
		reviewer,
		informative,
		executor,
		officer
	}
}