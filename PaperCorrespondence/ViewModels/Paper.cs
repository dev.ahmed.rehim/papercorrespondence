﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PaperCorrespondence.ViewModels
{
	public class Paper
	{
		public int Id { get; set; }
		public string Name { get; set; }
		[AllowHtml]
		[UIHint("tinymce_jquery_full")]
		public string Content { get; set; }
	}
}