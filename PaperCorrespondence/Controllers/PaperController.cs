﻿using PaperCorrespondence.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PaperCorrespondence.Views.Home
{
    public class PaperController : Controller
    {
        // GET: Paper
        public ActionResult Index()
        {
            return View();
        }

        // GET: Paper/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Paper/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Paper/Create
        [HttpPost]
        public ActionResult Create(Paper papr)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Paper/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Paper/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Paper/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Paper/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		public ActionResult Upload()
		{
			var file = Request.Files["Filedata"];
			string extension = Path.GetExtension(file.FileName);
			string fileid = Guid.NewGuid().ToString();
			fileid = Path.ChangeExtension(fileid, extension);

			string savePath = Server.MapPath(@"~\Uploads\" + fileid);
			file.SaveAs(savePath);

			return Content(Url.Content(@"~\Uploads\" + fileid));
		}
	}
}
