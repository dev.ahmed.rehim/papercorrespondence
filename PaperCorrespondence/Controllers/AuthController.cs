﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PaperCorrespondence.Controllers
{
    public class AuthController : Controller
    {
		// GET: Auth
		[HttpPost]
		public ActionResult Login(string Email, string Password, bool RemeberMe = false, string ReturnUrl = "Home/Index")
		{
			if (Request.IsAuthenticated)
				return RedirectToAction("Index", "Home");

			string Msg = "البريد او كلمه المرور غير صحيحه";
			var usr = new { Id="123",Roles="role1,role2"};// db.Client.FirstOrDefault(u => u.Email == Email && u.Password == Password);
			if (usr != null)
			{
				var ticket = new FormsAuthenticationTicket(version: 1, name: usr.Id, issueDate: DateTime.Now, expiration: DateTime.Now.AddSeconds(20000), isPersistent: false, userData: usr.Roles);
				var encryptedTicket = FormsAuthentication.Encrypt(ticket);
				var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
				this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

				return RedirectToAction("Index", "Home");
			}
			else
			{
				ViewBag.Msg = Msg;
			}
			return View();
		}


		[Authorize]
		public ActionResult Logout(string Url = "#")
		{
			FormsAuthentication.SignOut();
			return Redirect(Url);
		}
	}
}